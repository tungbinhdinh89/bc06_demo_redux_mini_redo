import './App.css';
import DemoMiniRedux from './DemoMiniRedux/DemoMiniRedux';

function App() {
  return (
    <div className="App">
      <DemoMiniRedux />
    </div>
  );
}

export default App;
