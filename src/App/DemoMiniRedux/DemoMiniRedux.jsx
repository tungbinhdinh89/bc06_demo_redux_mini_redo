import React, { Component } from 'react';
import { connect } from 'react-redux';

class DemoMiniRedux extends Component {
  render() {
    console.log('props', this.props);
    return (
      <div>
        <button
          onClick={() => {
            this.props.handleChangeNumber(-1);
          }}
          className="btn btn-danger">
          -
        </button>
        <strong className="mx-5">{this.props.Number}</strong>
        <button
          onClick={() => {
            this.props.handleChangeNumber(1);
          }}
          className="btn btn-success">
          +
        </button>
      </div>
    );
  }
}
let mapStateToProps = (state) => {
  return {
    Number: state.soLuong.number,
  };
};
let mapDispatchToProps = (dispatch) => {
  return {
    handleChangeNumber: (option) => {
      let action = {
        type: 'CHANGE_NUMBER',
        payload: option,
      };
      dispatch(action);
    },
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(DemoMiniRedux);
