let initialState = {
  number: 1,
};

export const demoMiniReducer = (state = initialState, action) => {
  switch (action.type) {
    case 'CHANGE_NUMBER': {
      return { ...state, number: state.number + action.payload };
    }
    default: {
      return state;
    }
  }
};
