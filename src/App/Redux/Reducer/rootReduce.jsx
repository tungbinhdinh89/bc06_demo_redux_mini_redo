import { combineReducers } from 'redux';
import { demoMiniReducer } from './demoMiniReducer';

export const rootReducer_demo = combineReducers({
  soLuong: demoMiniReducer,
});
